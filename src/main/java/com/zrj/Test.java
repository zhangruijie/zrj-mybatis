package com.zrj;

import com.zrj.mybatis.entity.User;
import com.zrj.mybatis.mapper.UserMapper;
import com.zrj.mybatis.zrjMybatis.session.MySqlSession;
import com.zrj.mybatis.zrjMybatis.session.MySqlSessionFactory;
import com.zrj.mybatis.zrjMybatis.session.MySqlSessionFactoryBuilder;

import java.io.InputStream;
import java.util.List;

public class Test {

    public static void main(String[] args) {

        //读取mybatis-config.xml 配置文件
        InputStream inputStream = Test.class.getClassLoader().
                getResourceAsStream("mybatis-config.xml");
        //通过SqlSessionFactoryBuilder 来构建sqlSessionFacoty工厂
        MySqlSessionFactory mySqlSessionFactory=new MySqlSessionFactoryBuilder().build(inputStream);
       //通过mysqlSessionFactory 来打开sqlSession
        MySqlSession mySqlSession=mySqlSessionFactory.openSession();

        //获取mapper接口对象
        UserMapper userMapper=mySqlSession.getMapper(UserMapper.class);
        //通过UserMapper的代理对象取执行方法
        List<User> users = userMapper.selectUser("1");
        System.out.println(users);
    }
}
