package com.zrj.mybatis.mapper;


import com.zrj.mybatis.entity.User;

import java.util.List;

public interface UserMapper {

    List<User> selectUser(String id);
}
