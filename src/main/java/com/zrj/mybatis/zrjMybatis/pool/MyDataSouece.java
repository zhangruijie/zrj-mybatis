package com.zrj.mybatis.zrjMybatis.pool;

import com.zrj.mybatis.zrjMybatis.mapper.MyConfiguration;
import com.zrj.mybatis.zrjMybatis.mapper.MyEnvironment;

import javax.sql.DataSource;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.SQLFeatureNotSupportedException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

public class MyDataSouece implements DataSource {

    private MyEnvironment myEnvironment;
    private List<Connection> pool;
    private Connection conn=null;
    private static MyDataSouece instance=null;
    private static  final int POOL_SIZE=15;

    public MyDataSouece(MyEnvironment myEnvironment) {
        this.myEnvironment = myEnvironment;
        pool=new ArrayList<Connection>(POOL_SIZE);
        this.createConnection();
    }

    /**
     * 创建初始的数据库连接
     */
    private void createConnection() {
     for (int i=0;i<POOL_SIZE;i++){
         try {
             Class.forName(myEnvironment.getDriver());
             conn=DriverManager.getConnection(myEnvironment.getUrl(),
                     myEnvironment.getUsername(),myEnvironment.getPassword());
             pool.add(conn);
         }catch (Exception e){
          e.getStackTrace();
         }
     }
    }

    /**
     * 得到当前连接池的一个实例
     * @param myEnvironment
     * @return
     */
    public static MyDataSouece getInstance(MyEnvironment myEnvironment){
        if (instance==null){
            instance=new MyDataSouece(myEnvironment);
        }
        return instance;
    }

    @Override
    public synchronized Connection getConnection() throws SQLException {
        if (pool.size()>0){
            Connection connection = pool.get(0);
            pool.remove(connection);
            return connection;
        }else {
            return null;
        }
    }


    /**
     * 用完将连接放回连接池
     */
    public synchronized void release(Connection conn){
        pool.add(conn);
    }

    /**
     * 关闭连接池中的所有连接
     */
    public synchronized  void closePool(){
        for (int i=0;i<pool.size();i++){
            try {
               conn= pool.get(i);
               conn.close();
               pool.remove(i);
            }catch (Exception e){
             e.getStackTrace();
            }
        }
    }

    @Override
    public Connection getConnection(String username, String password) throws SQLException {
        return null;
    }

    @Override
    public <T> T unwrap(Class<T> iface) throws SQLException {
        return null;
    }

    @Override
    public boolean isWrapperFor(Class<?> iface) throws SQLException {
        return false;
    }

    @Override
    public PrintWriter getLogWriter() throws SQLException {
        return null;
    }

    @Override
    public void setLogWriter(PrintWriter out) throws SQLException {

    }

    @Override
    public void setLoginTimeout(int seconds) throws SQLException {

    }

    @Override
    public int getLoginTimeout() throws SQLException {
        return 0;
    }

    @Override
    public Logger getParentLogger() throws SQLFeatureNotSupportedException {
        return null;
    }
}
