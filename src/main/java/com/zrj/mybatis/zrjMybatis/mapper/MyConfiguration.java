package com.zrj.mybatis.zrjMybatis.mapper;


import java.util.Map;

public class MyConfiguration {

    //mybatis-config.xml
    private MyEnvironment environment;  //mybatis 运行时的环境对象
    //xxMapper.xml
    private Map<String,MapperStatement> mapperStatementMap; //用来存放mypper.xml中的映射对象

    public MyEnvironment getEnvironment() {
        return environment;
    }

    public void setEnvironment(MyEnvironment environment) {
        this.environment = environment;
    }

    public Map<String, MapperStatement> getMapperStatementMap() {
        return mapperStatementMap;
    }



    public void setMapperStatementMap(Map<String, MapperStatement> mapperStatementMap) {
        this.mapperStatementMap = mapperStatementMap;
    }
}
