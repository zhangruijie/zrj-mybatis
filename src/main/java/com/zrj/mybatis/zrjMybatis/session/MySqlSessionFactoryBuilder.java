package com.zrj.mybatis.zrjMybatis.session;

import com.zrj.mybatis.zrjMybatis.mapper.MyConfiguration;
import com.zrj.mybatis.zrjMybatis.util.XPathParser;
import com.zrj.mybatis.zrjMybatis.util.XmlConfigBuilder;
import org.dom4j.DocumentException;

import java.io.InputStream;

public class MySqlSessionFactoryBuilder {

    public  MySqlSessionFactory build(InputStream inputStream){

        //mybatis的配置类,从mybatis-config.xml中读取出来的信息，保存到这个对象中
        MyConfiguration myConfiguration= null;
        try {
            myConfiguration = new XPathParser(inputStream).parse();
        } catch (DocumentException e) {
            e.printStackTrace();
        }

        return new MySqlSessionFactory(myConfiguration);
    }
}
