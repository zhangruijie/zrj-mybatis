package com.zrj.mybatis.zrjMybatis.session;

import com.zrj.mybatis.zrjMybatis.executor.MyExecutor;
import com.zrj.mybatis.zrjMybatis.mapper.MyConfiguration;

public class MySqlSessionFactory {

    private MyConfiguration myConfiguration;

    public MySqlSessionFactory(MyConfiguration myConfiguration){
        this.myConfiguration=myConfiguration;
    }

    public MySqlSession openSession() {
        MyExecutor myExecutor=new MyExecutor(myConfiguration);
       return new MySqlSession(myExecutor,myConfiguration);
    }
}
