package com.zrj.mybatis.zrjMybatis.session;

import com.zrj.mybatis.mapper.UserMapper;
import com.zrj.mybatis.zrjMybatis.executor.MyExecutor;
import com.zrj.mybatis.zrjMybatis.mapper.MapperStatement;
import com.zrj.mybatis.zrjMybatis.mapper.MyConfiguration;
import com.zrj.mybatis.zrjMybatis.proxy.MapperProxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;
import java.util.List;
import java.util.concurrent.Executor;

public class MySqlSession {

    private MyExecutor executor; //mybatis 的执行器

    private MyConfiguration myConfiguration;

    public MySqlSession(MyExecutor executor, MyConfiguration myConfiguration) {
        this.executor = executor;
        this.myConfiguration = myConfiguration;
    }

    public MyExecutor getExecutor() {
        return executor;
    }

    public void setExecutor(MyExecutor executor) {
        this.executor = executor;
    }

    public MyConfiguration getMyConfiguration() {
        return myConfiguration;
    }

    public void setMyConfiguration(MyConfiguration myConfiguration) {
        this.myConfiguration = myConfiguration;
    }

    public <T> T getMapper(Class<T> clazz) {
        MapperProxy mapperProxy = new MapperProxy(this);
        return (T)Proxy.newProxyInstance(clazz.getClassLoader(),
               new Class<?>[] {clazz},mapperProxy);
    }



    public <T> List<T> query(String statementKey,Object args) {
        MapperStatement mapperStatement = myConfiguration.getMapperStatementMap().get(statementKey);
       return executor.query(mapperStatement,args);
    }
}
