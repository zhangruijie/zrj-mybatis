package com.zrj.mybatis.zrjMybatis.proxy;

import com.zrj.mybatis.zrjMybatis.session.MySqlSession;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.Collection;
import java.util.Map;

/**
 * mapper 的代理类，代理真实的mapper去执行方法
 */
public class MapperProxy implements InvocationHandler {
    private MySqlSession sqlSession;

    public MapperProxy(MySqlSession sqlSession) {
        this.sqlSession = sqlSession;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
       if (Collection.class.isAssignableFrom(method.getReturnType())){
           //如果查询的方法返回值为集合或者子集，类型，表示查询多条数据
           String statementKey=method.getDeclaringClass().getName()+"."+method.getName();
           return sqlSession.query(statementKey,(null==args)?null:args[0]);
       }else if (Map.class.isAssignableFrom(method.getReturnType())){
           //表示返回值为map类型存放
       }else {
           //返回对象
           String statementKey=method.getDeclaringClass().getName()+"."+method.getName();
           return sqlSession.query(statementKey,(null==args)?null:args[0]).get(0);
       }
       return null;
    }
}
