package com.zrj.mybatis.zrjMybatis.reflect;

import java.lang.reflect.Field;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

public class RefelctUtill {


    /**
     * 将结果集映射到实体类对象中
     * @param entity  实体类
     * @param resultSet  结果集
     * @throws SQLException
     */
    public static void setPreToBeanFromResult(Object entity, ResultSet resultSet) throws SQLException {
        ResultSetMetaData metaData = resultSet.getMetaData();
        int columnCount = metaData.getColumnCount();
        Field[] declaredFields = entity.getClass().getDeclaredFields();
        for (int i=0;i<columnCount;i++){
            //获取result结果集中的列名转成大写
            String columName = metaData.getColumnName(i + 1).replace("_", "").toUpperCase();
           for (int j=0;j<declaredFields.length;j++){
               //获取entity实体类中的列名转成大写
               String fieldName = declaredFields[j].getName().toUpperCase();
               if (columName.equalsIgnoreCase(fieldName)){
                   if (declaredFields[j].getType().getSimpleName().equals("Integer")){
                       setPreToBean(entity,declaredFields[j].getName(),
                               resultSet.getInt(metaData.getColumnName(i+1)));
                   }else if (declaredFields[j].getType().getSimpleName().equals("Long")){
                       setPreToBean(entity,declaredFields[j].getName(),
                               resultSet.getLong(metaData.getColumnName(i+1)));
                   }else if (declaredFields[j].getType().getSimpleName().equals("String")){
                       setPreToBean(entity,declaredFields[j].getName(),
                               resultSet.getString(metaData.getColumnName(i+1)));
                   }else if (declaredFields[j].getType().getSimpleName().equals("Date")){
                       setPreToBean(entity,declaredFields[j].getName(),
                               resultSet.getDate(metaData.getColumnName(i+1)));
                   }else if (declaredFields[j].getType().getSimpleName().equals("Boolean")){
                       setPreToBean(entity,declaredFields[j].getName(),
                               resultSet.getBoolean(metaData.getColumnName(i+1)));
                   }else if (declaredFields[j].getType().getSimpleName().equals("BigDecimal")){
                       setPreToBean(entity,declaredFields[j].getName(),
                               resultSet.getBigDecimal(metaData.getColumnName(i+1)));
                   }
                   break;
               }

           }
        }
    }

    /**
     * 属性值设置到bean对象中
     */
    private static void setPreToBean(Object entity, String name, Object value) {

     try {
         Field field = entity.getClass().getDeclaredField(name);
         field.setAccessible(true);
         field.set(entity,value);
     }catch (Exception e){

     }
    }
}
