package com.zrj.mybatis.zrjMybatis.util;

import com.zrj.mybatis.zrjMybatis.mapper.MapperStatement;
import com.zrj.mybatis.zrjMybatis.mapper.MyConfiguration;
import com.zrj.mybatis.zrjMybatis.mapper.MyEnvironment;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.Node;
import org.dom4j.io.SAXReader;
import org.dom4j.tree.DefaultDocument;

import javax.xml.xpath.XPath;
import java.io.File;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;

/**
 * XPath文档解析。jdk自带的
 * 以及结合dom4j 第三方解析工具 实现
 */
public class XPathParser {

    private  XPath xPath;
    private static Document document;

    private String xpath="//property[@name]";  //获取mybatis-config.xml中的所有的property节点

    private String mapperPath="//mapper[@resource]";

    public XPathParser(InputStream inputStream) {
        SAXReader reader = new SAXReader();
        try {
            document= reader.read(inputStream);
        } catch (DocumentException e) {
            e.printStackTrace();
        }
    }

    public XPath getxPath() {
        return xPath;
    }

    public void setxPath(XPath xPath) {
        this.xPath = xPath;
    }

    public Document getDocument() {
        return document;
    }


    public MyConfiguration parse() throws DocumentException {
        MyConfiguration myConfiguration = new MyConfiguration();
        MapperStatement mapperStatement = new MapperStatement();
        HashMap<String, MapperStatement> statementHashMap = new HashMap<String, MapperStatement>();
        MyEnvironment myEnvironment = new MyEnvironment();
        List<Node>  propertys = document.selectNodes(xpath);
        for (Node prpperty:propertys){
            //获取当前节点的name属性的值
            Element elementName=(Element)prpperty.selectObject(".[@name]");
            String name = elementName.attribute("name").getValue();
            //获取当前节点的value属性的值
            Element elementValue=(Element)prpperty.selectObject(".[@value]");
            String value = elementValue.attribute("value").getValue();
            if (name.equals("driver")){
                myEnvironment.setDriver(value);
            }
            if (name.equals("url")){
                myEnvironment.setUrl(value);
            }
            if (name.equals("username")){
                myEnvironment.setUsername(value);
            }
            if (name.equals("password")){
                myEnvironment.setPassword(value);
            }
        }
        myConfiguration.setEnvironment(myEnvironment);
        List<Node> mapperList = document.selectNodes(mapperPath);
        for (Node mapper:mapperList){   //循环mapper.xml文件
            Element element=(Element) mapper.selectObject(".[@resource]");
            //获取到mapper.xml的文件路径
            String resource = element.attribute("resource").getValue();
            InputStream inputStream = XPathParser.class.getClassLoader().getResourceAsStream(resource);
            Document resourceDocument = new SAXReader().read(inputStream);
            Element mapperxmlMapperNode=(Element) resourceDocument.selectObject("//mapper");
            String namespace = mapperxmlMapperNode.attribute("namespace").getValue();
            List<Node> selectNodes = resourceDocument.selectNodes("//select");
            for (Node selectNode:selectNodes){
               Element currenyNode= (Element)selectNode.selectObject(".[@id]");
                String mapperSelectId = currenyNode.attribute("id").getValue();
                String mapperSelectParameterType = currenyNode.attribute("parameterType").getValue();
                String mapperSelectResultType = currenyNode.attribute("resultType").getValue();
                String mapperSelectSql = currenyNode.getText().trim();
                mapperStatement.setNamespace(namespace);
            mapperStatement.setId(mapperSelectId);
            mapperStatement.setParameterType(mapperSelectParameterType);
            mapperStatement.setResultType(mapperSelectResultType);
            mapperStatement.setSql(mapperSelectSql);
            statementHashMap.put(namespace+"."+mapperSelectId,mapperStatement); //将mapper.xml 中namespace 和select标签的id用.拼接作为key
            myConfiguration.setMapperStatementMap(statementHashMap);
            }
         }
        return myConfiguration;
    }
}
