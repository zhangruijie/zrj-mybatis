package com.zrj.mybatis.zrjMybatis.util;

/**
 * sql转换，解析  将  id=#{id}   转换成 id = ?   ,不然预编译对象执行不了
 */
public class SqlTokenPaser {
    private static final String openToken="#{";
    private static final String closeToken="}";

    public static String parse(String sql){
        if (sql==null || sql.isEmpty()){
            return "";
        }
        int start = sql.indexOf(openToken);
        if (start==-1){
            return sql;
        }
        char[] src = sql.toCharArray();
        int offset=0;
        final StringBuilder builder = new StringBuilder();
        StringBuilder expression=null;
        while (start>-1){
            if (start>0 && src[start-1]=='\\'){
                builder.append(src,offset,start-offset-1).append(openToken);
                offset=start+openToken.length();
            }else {
                if (expression==null){
                    expression=new StringBuilder();
                }else {
                    expression.setLength(0);
                }
                builder.append(src,offset,start-offset);
                offset=start+openToken.length();
                int end=sql.indexOf(closeToken,offset);
                while (end>-1){
                    if (end>offset && src[end-1]=='\\') {
                        expression.append(src, offset, end - offset - 1).append(openToken);
                        offset = end + closeToken.length();
                    }else {
                        expression.append(src,offset,end-offset);
                        offset=end+closeToken.length();
                        break;
                    }
                }
                if (end==-1){
                    builder.append(src, start,src.length-start);
                    offset=src.length;
                }else {
                    builder.append("?");
                    offset=end+closeToken.length();
                }
            }
            start=sql.indexOf(openToken,offset);
        }
        if (offset<src.length){
            builder.append(src,offset,src.length-offset);
        }
        return builder.toString();
    }

    public static void main(String[] args) {
        String sql=" select * from t_user where id = #{id}";
        String parse = parse(sql);
        System.out.println(parse);
    }

}
