package com.zrj.mybatis.zrjMybatis.executor;

import com.zrj.mybatis.zrjMybatis.reflect.RefelctUtill;
import com.zrj.mybatis.zrjMybatis.mapper.MapperStatement;
import com.zrj.mybatis.zrjMybatis.mapper.MyConfiguration;
import com.zrj.mybatis.zrjMybatis.pool.MyDataSouece;
import com.zrj.mybatis.zrjMybatis.util.SqlTokenPaser;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class MyExecutor {

    private MyDataSouece myDataSouece;

    public MyExecutor(MyConfiguration myConfiguration){
    myDataSouece= MyDataSouece.getInstance(myConfiguration.getEnvironment());
    }

    public <T> List<T> query(MapperStatement mapperStatement, Object paramter) {
        Connection conn=null;
        PreparedStatement psmt=null;
        ResultSet resultSet=null;
        List<T> resultList=new ArrayList<>();
        try {
            conn=myDataSouece.getConnection();
            psmt = conn.prepareStatement(SqlTokenPaser.parse(mapperStatement.getSql()));
            if (paramter instanceof String){
                psmt.setString(1,(String)paramter);
            }else if (paramter instanceof Integer){
                psmt.setInt(1,(Integer) paramter);
            }else if (paramter instanceof Long){
                psmt.setLong(1,(Long) paramter);
            }
            resultSet=psmt.executeQuery();
            //处理结果集，转换成对象
            handlerResultSetToEntity(resultSet,resultList,mapperStatement.getResultType());
        }catch (Exception e){
           e.getStackTrace();
        }
        return resultList;
    }


    /**
     * 处理结果集，将数据映射到entity实体类中
     * @param resultSet
     * @param resultList
     * @param resultType
     * @param <T>
     */
    private <T> void handlerResultSetToEntity(ResultSet resultSet,List<T> resultList,String resultType){
        try {
            Class<T> aClass = (Class<T>)Class.forName(resultType);
            while (resultSet.next()){
                Object entity = aClass.newInstance();
                //将结果集封装到 entity实体类中
                RefelctUtill.setPreToBeanFromResult(entity,resultSet);
                //将实体类加入集合中
                resultList.add((T)entity);
            }
        }catch (Exception e){

        }

    }
}
